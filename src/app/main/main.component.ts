import { Component, ViewChild, AfterViewInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { Browser } from '@syncfusion/ej2-base';
import { SidebarComponent } from '@syncfusion/ej2-angular-navigations';

import { AuthenticationService } from '../login/_services';
import { DataService } from '../data.service';

import { User } from '../login/_models'

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements AfterViewInit {
  @ViewChild('sideBar')
  public sideBar: SidebarComponent;
  public showBackdrop: Boolean = false;
  public closeOnDocumentClick: Boolean = false;

  currentUser: User;
  person: Object;

  constructor(
    private authenticationService: AuthenticationService,
    private dataService: DataService,
    private router: Router) {
    document.body.classList.add('main-page');
    this.currentUser = this.authenticationService.currentUser;
    if (Browser.isDevice) {
      this.showBackdrop = true;
      this.closeOnDocumentClick = true;
    } 
    this.readPerson();
  }

  ngAfterViewInit() {
    if (Browser.isDevice) {
      document.querySelector('.planner-header').classList.add('device-header');
      document.querySelector('.planner-wrapper').classList.add('device-wrapper');
    }
  }

  btnClick() {
    this.sideBar.show();
  }

  onItemClick(args: any) {
    if (Browser.isDevice) {
      this.sideBar.hide();
    }
    const elements: HTMLElement[] = args.currentTarget.parentElement.querySelectorAll('.active-item');
    elements.forEach(element => {
      if (element.classList.contains('active-item')) { element.classList.remove('active-item'); }
    });
    args.currentTarget.classList.add('active-item');
  }

  onLoggedIn(user: User) {
    console.log(user);
    this.currentUser = user;
    this.readPerson();
  }

  onLogout() {
    this.authenticationService.logout();
    this.currentUser = null;
    this.router.navigate(['/']);
  }

  readPerson() {
    if (this.currentUser != null)  {
      this.person = this.authenticationService.readPerson(this.currentUser);
    }
  }
}
