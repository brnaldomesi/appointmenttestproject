﻿export class User {
    Id: number;
    UserId: string;
    UserType: string;
    PersonId: number;
    PIN: string;
}