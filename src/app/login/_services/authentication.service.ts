﻿import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { DataService } from '../../data.service';
import { User } from '../_models';

@Injectable({ providedIn: 'root' })
export class AuthenticationService {
    public currentUser: User;

    constructor(private dataService: DataService) {
        this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    }

    public get currentUserValue(): User {
        return this.currentUser;
    }

    login(username: string, password: string) {
        let user = this.dataService.auth(username, password);
        if (user) {
            localStorage.setItem('currentUser', JSON.stringify(user));
            this.currentUser = user;
        }
        return user;
    }

    logout() {
        // remove user from local storage to log user out
        // console.log('logout');
        localStorage.removeItem('currentUser');
        this.currentUser = null;
    }

    readPerson(user) {
        let person;
        if (user == null)
            return null;
        if (user.UserType == 'Admin') {
            person = {Name: 'Admin', Text: 'Admin'};
            // console.log(person);
        } else {
            // console.log(this.currentUser);
            person = this.dataService.getPersonData(this.currentUser.UserType, this.currentUser.PersonId);
            // console.log(person);
        }
        return person;
    }
}