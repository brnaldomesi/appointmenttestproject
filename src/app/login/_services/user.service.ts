﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { User } from '../_models';
import { DataService } from '../../data.service';

@Injectable({ providedIn: 'root' })
export class UserService {
    constructor(private http: HttpClient) { }

    getAll() {
        // return this.http.get<User[]>(`${config.apiUrl}/users`);
    }

    getById(id: number) {
        // return this.http.get(`${config.apiUrl}/users/${id}`);
    }

    register(user) {
        let result;
        result.err = 0;
        result.msg = '';
        return result;
        // return this.http.post(`${config.apiUrl}/users/register`, user);

    }

    update(user: User) {
        // return this.http.put(`${config.apiUrl}/users/${user.id}`, user);
    }

    delete(id: number) {
        // return this.http.delete(`${config.apiUrl}/users/${id}`);
    }
}